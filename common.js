(function ($) {

/* Support for array.indexOf if it doesn't supports natively */
if(!Array.prototype.indexOf){Array.prototype.indexOf=function(a){"use strict";if(this==null){throw new TypeError}var b=Object(this);var c=b.length>>>0;if(c===0){return-1}var d=0;if(arguments.length>1){d=Number(arguments[1]);if(d!=d){d=0}else if(d!=0&&d!=Infinity&&d!=-Infinity){d=(d>0||-1)*Math.floor(Math.abs(d))}}if(d>=c){return-1}var e=d>=0?d:Math.max(c-Math.abs(d),0);for(;e<c;e++){if(e in b&&b[e]===a){return e}}return-1}}

/* AJAX FUNCTIONS */
/* @autoAjaxRequest=1: Sends the autoajax request */
function ajaxRequest(strDivId, strRequestUrl, options){
	options = options || {};
	var defaultOptions = {url: strRequestUrl, type: 'POST', beforeSend: function(request,options){showLoadingImage(strDivId);}, success: function(html){$('#'+strDivId).html(html); removeLoadingImage(strDivId); bindEssentials();}};

	if( typeof(options.autoRequest) != "undefined" && 1 == options.autoRequest ) {
		return sendAutoAjaxRequest(strDivId, strRequestUrl, options);
	}

	options = $.extend({},defaultOptions,options);
	$.ajax(options);
	bindEssentials();
}
this.ajaxRequest = ajaxRequest; // will need to see if it's really needed.[KAL] 

function showLoadingImage(strDivId){
	var divWidth;
	if ($("#"+strDivId).width() < 10) {divWidth = "100%"; } else { divWidth = $("#"+strDivId).width()+'px'; }
	var strBumperImage = '<div id="'+strDivId+'_ajxOverlayImage" class="loading-overlay" style="width: '+divWidth+'; height: '+$("#"+strDivId).height()+'px; min-height:160px; display:block;';
	strBumperImage += '"><img src="/images/loader.jpg" style="position:absolute; top:45%; right:50%;">';
	strBumperImage += '</div>';
	$('#'+strDivId).prepend(strBumperImage);
	
	/* For loadDialog */
	var isdialog = jQuery("#"+strDivId).closest('div.ui-dialog');
	if( isdialog.length > 0 ) {	jQuery( "#"+strDivId+'_ajxOverlayImage' ).css({ height:isdialog.innerHeight() - 40 +'px',	width:isdialog.innerWidth() - 10 +'px' }); } 

}
this.showLoadingImage = showLoadingImage;

function removeLoadingImage(strDivId){
	if( $( "#"+strDivId + '_ajxOverlayImage' )){ $('#'+strDivId+'_ajxOverlayImage').remove();}
}
this.removeLoadingImage = removeLoadingImage;

function sendAutoAjaxRequest(strDivId, strRequestUrl, options){
	options = options || {};
	var defaultOptions = { url: strRequestUrl, type: 'POST', minChars: 5,
						   beforeSend: function(request,options){$('#'+strDivId).hide();$('#'+strDivId).html($('#'+options.loadingDivId).html());$('#'+strDivId).fadeIn('slow');}, success: function(html){$('#'+strDivId).html(html);}
						 };

	options = $.extend({},defaultOptions,options);

	if( ($('#'+options.originId).val()).length >= options.minChars) {
		$.ajax(options);
	}
}
this.sendAutoAjaxRequest = sendAutoAjaxRequest;

/* FUNCTION FOR SHOWING SUCCESS MESSAGES IN COMMON DIV */
showSuccessMessages = function( msg ) {
	if( 0 < $('#show_msg_inner').length ) {
		if( 0 == $('#para_show_msg').length ) {
			$('#show_msg_inner').html('<p id=\'para_show_msg\' class = \'alert success slim\'>'+msg+'</p>');
			$('#show_msg_inner').fadeIn().animate({opacity: 1.0}, 7000).fadeOut(500,function(){jQuery('#para_show_msg').remove()});
		}
	} else {
		if( 0 == $('#para_show_msg').length ) {
			$('#show_msg_outer').html('<p id=\'para_show_msg\' class = \'alert success slim\'>'+msg+'</p>');
			$('#show_msg_outer').fadeIn().animate({opacity: 1.0}, 7000).fadeOut(500,function(){jQuery('#para_show_msg').remove()});
		}
	}
}

this.showSuccessMessages = showSuccessMessages;

function loadDialog(wdt, ht, url, divId, title,  minWdt, maxHt, options){
	var modalWidth;
	var dialog;

	var options = options || {};	
	var defaultOptions = {onCloseCallback: function(){}};
	options = $.extend({},defaultOptions,options);
	
	if (wdt == "auto") {
		if ($(window).width() > 1040) { if ($(window).width() < 1560) { modalWidth = $(window).width() - 60; } else { modalWidth = 1500; } } else { modalWidth = 980; }
	} else {
		modalWidth = wdt;
	}

	var modalHeight;
	if (ht == "auto") {
		if (maxHt) {
			if (maxHt > 1000) { maxHt = 1000 }
			if ($(window).height() > 640) {
				if ($(window).height() < maxHt) { modalHeight = $(window).height() - 60; } else { modalHeight = maxHt; }
			} else {
				modalHeight = 580;
			}

		} else {
			if ($(window).height() > 640) { if ($(window).height() < 1060) { modalHeight = $(window).height() - 60; } else { modalHeight = 1000; } } else { modalHeight = 580; }
		}
	} else {
		modalHeight = ht;
	}
	
	if( !divId ) divId = 'mod';
	if( $('#'+divId).length > 0 ){ dialog = $('#'+divId); } else { dialog = $("<div id='"+divId+"' title='"+title+"'></div>").appendTo('body'); }
	
	options = $.extend({},{open:function(event, ui){ $("html").css("overflow", "hidden"); $(".ui-widget-overlay").width("100%"); $('html').unbind("keydown");}, autoOpen:true, width:modalWidth, minWidth:minWdt, height:modalHeight, title:title, modal:true, close:function(event, ui){ $(this).queue(function(){$(this).remove()}).delay(100).queue(function(){if( 'undefined' == typeof event.originalEvent )eval(options.onCloseCallback)()}).delay(100).queue(function(){bindLargeDialogEscape()});  if ($(".modal-large").length == 0){$("html").css("overflow", "");}}}, options);

	if( 0 != parseInt(wdt) && 0 != parseInt(ht) ) {
		dialog.dialog(options);
	}
	
	showLoadingImage(divId);
	dialog.load(url);
	return dialog;
}
this.loadDialog = loadDialog;

var largeDialogOptions = {};
var dialogContentCount = 0;

function loadLargeDialog(url, options, title){

	var curScrollTop = $("html").scrollTop();
	var defaultOptions = {onCloseCallback: function(){}};
	var options = options || {};
	options = $.extend({},defaultOptions,options);
	
	largeDialogOptions = options;
	
	var modalHeight = $(window).height();
	var overlayHeight = $(window).height()-50;

	//var logoClassName = document.getElementById('logo_class_name').value;
	var logoClassName = "entrata-logo";

	var modal = '<div class="modal-head"><div class="'+logoClassName+'"></div>';
		if (title) {
			//if( logoClassName == 'logo') {
				//modal += '<div class="title" style="left:220px"><span><i></i><a class="nohref" onclick="closeModalContent(0);">'+title+'</a></span></div>';
			//} else {
				modal += '<div class="title"><span><i></i><a class="nohref" onclick="closeModalContent(0);">'+title+'</a></span></div>';
			//}
		}
		modal += '<i class="close" onclick="closeLargeDialog();"></i>'
		modal += '<div class="close-menu"><a class="nohref" id="close-current" onclick="closeLargeDialog(\'current\');">Close [1]</a><br></br><a class="nohref" id="close-all" onclick="closeLargeDialog(\'all\');">Close All [A]</a></div></div>';
		modal += '<div class="modal-large" style="height:'+modalHeight+'px;">';
		modal += '<div></div>';
		modal += '<div class="modal-body-container" id="modal_container0" style="height:'+overlayHeight+'px"><div class="modal-body" id="modal_body"></div></div>';
		modal += '<div class="modal-overlay" style="height:'+overlayHeight+'px"></div>';
	modal += '</div>';
	
	$("body").append(modal);
	
	$("html").css("overflow", "hidden");

	bindLargeDialogEscape();

	ajaxRequest("modal_body", url, '');	
	dialogContentCount = 0;
	$("html").scrollTop(curScrollTop);
}
this.loadLargeDialog = loadLargeDialog;


function addDialogContent(url, title){
	//if (dialogContentCount == 0) {$("#modal_body").hide();} else {$("#modal_body"+dialogContentCount).hide();}
	var overlayHeight = $(window).height()-50;
	var containerIndex = parseInt($("#modal_container"+dialogContentCount).css("z-index"))+2;
	var overlayIndex = parseInt($("#modal_container"+dialogContentCount).css("z-index"))+1;
	dialogContentCount += 1;
	$(".modal-head .title").append('<span><i></i><a class="nohref" onclick="closeModalContent('+dialogContentCount+');">'+title+'</a></span>');
	$(".modal-large").prepend('<div id="modal_container'+dialogContentCount+'" class="modal-body-container hide" style="z-index:'+containerIndex+'; height:'+overlayHeight+'px"><div class="modal-body"><div id="modal_body'+dialogContentCount+'"></div></div></div><div class="modal-overlay" id="modal_overlay'+dialogContentCount+'" style="z-index:'+overlayIndex+'; height:'+overlayHeight+'px"></div>');
	$(".modal-large").find("#modal_container"+dialogContentCount).slideDown(700);
	ajaxRequest("modal_body"+dialogContentCount, url, '');	
}
this.addDialogContent = addDialogContent;

function closeModalContent(contentNum){
	var maxCount = $(".modal-head .title span").size();
	for (i=contentNum+1; i<maxCount; i++) {
		$(".modal-head .title span").eq(contentNum+1).remove();
		$("#modal_container"+i).slideUp(700);
		$("#modal_container"+i).promise().done(function(){ $(this).remove(); });
		$("#modal_overlay"+i).fadeOut(1000);
		$("#modal_overlay"+i).promise().done(function(){ $(this).remove(); });
	}
	
	dialogContentCount = contentNum;
}
this.addDialogContent = addDialogContent;

function bindLargeDialogEscape(){
	$('html').unbind("keydown.esc").bind('keydown.esc', 'esc', function(e){closeLargeDialog(); e.preventDefault()});
}

this.bindLargeDialogEscape = bindLargeDialogEscape;

function closeLargeDialog(options){
	jQuery(document).scrollTop(jQuery('body').attr('data-vertical-scroll-position'));
	if (dialogContentCount < 1 || options == "all") {
		var curScrollTop = $("html").scrollTop();
		$("body").find(".modal-body").slideUp(400).queue(function(){ 
			$("body").find(".modal-large").fadeOut(200).queue(function(){
				$("body").find(".modal-head").remove();
				$("body").find(".modal-large").remove();
				$("html").css("overflow", "").queue(function(){$("html").scrollTop(curScrollTop); $(this).dequeue();});
			});
			$(this).remove();
		});
		
		if( 0 < $("body #notifyUnitSearchDiv").size() ) {
			$( "body #notifyUnitSearchDiv" ).remove();
		}
		
		$(".modal-head .close-menu").fadeOut("fast");
		if( largeDialogOptions && largeDialogOptions.onCloseCallback ) eval(largeDialogOptions.onCloseCallback)();
		
	} else {
		if (options == "current") {
			closeModalContent(dialogContentCount-1);
			$(".modal-head .close-menu").fadeOut("fast");
		} else {
			$(".modal-head .close-menu #close-current").text("Close "+ $(".modal-head .title span").eq(dialogContentCount).text() +" [1]");
			$(".modal-head .close-menu").toggle();
			if ($(".modal-head .close-menu").is(":visible")) {
				$('html').unbind("keydown.close1").bind('keydown.close1', '1', function(){closeLargeDialog('current'); $('html').unbind("keydown.close1");});
				$('html').unbind("keydown.closeAll").bind('keydown.closeAll', 'a', function(){closeLargeDialog('all'); $('html').unbind("keydown.closeAll");});
			} else {
				$('html').unbind("keydown.close1"); $('html').unbind("keydown.closeAll");
			}
		}
	}
}
this.closeLargeDialog = closeLargeDialog;

function loadSmallDialog(divId, url, options){
	
	var defaultOptions = {onCloseCallback: function(){}};
	var options = options || {};
	options = $.extend({},defaultOptions,options);
	
	divId = divId || 'small_dialog'; 
	
	largeDialogOptions = options;
	
	var modalHeight = $(window).height();
	var overlayHeight = $(window).height()-50;
	var modal =  '<div id='+ divId +' class="modal-large" style="height:'+modalHeight+'px;">';		
		modal += '<div class="modal-head"><div class="logo"></div></div>';
		modal += 	'<div class="modal-body-container" style="height:'+overlayHeight+'px"><div class="modal-body"><div id="modal_body"></div></div></div>';
		modal += 	'<div class="modal-overlay" style="height:'+overlayHeight+'px"></div>';
		modal += '</div>';
	
	$("body").append(modal);
	$("html").css("overflow", "hidden");

	bindSmallDialogEscape(divId);

	ajaxRequest("modal_body", url, '');	
}

function bindSmallDialogEscape(divId){
	$('html').unbind("keydown").bind({
		keydown: function(e){
			if (!$(".ui-dialog").is(":visible") && e.keyCode && e.keyCode === $.ui.keyCode.ESCAPE) {					
				closeSmallDialog(divId);
				e.preventDefault();
		    }
		}
	});
}

function closeSmallDialog(divId){
	$("body").find("#"+divId).hide().queue(function(){ 
		$("html").css("overflow", "auto");
		$(this).remove();
	});
	if( largeDialogOptions && largeDialogOptions.onCloseCallback ) eval(largeDialogOptions.onCloseCallback)();
}

/* UI FUNCTIONS */
function bindEssentials(){ 
	
	$(window).resize(function(){
		$(".modal-overlay").height($(window).height());
		$(".modal-large").height($(window).height());
		$(".modal-body-container").height($(window).height()-50);
	});
}

$(function(){
	// UI ESSENTIALS
	bindEssentials();	
});

resizeDialog = function(dialog, options) {
	options = options || {};
	var defaultOptions = {position: 'center'};
	options = jQuery.extend({},defaultOptions,options);
	
	dialog.dialog( "option", options);
	dialog.dialog( "option", "position", options.position );
}
resizeDialog = this.resizeDialog;

$(document).ready( function() {
	
	//Added for testing
	$('#main-content-header').click(function(event){	 
		//loadLargeDialog( 'http://cococo.lcl/ahah/advanced_popup/myphp' );
		//loadSmallDialog('panels-ipe-paneid-14', 'http://cococo.lcl/ahah/advanced_popup/myphp');
		//loadDialog( 1200 , 'auto', 'http://cococo.lcl/ahah/advanced_popup/myphp', 'add_edit_gl_entry', 'Add Journal Entry' );
		//ajaxRequest('panels-ipe-paneid-14', 'http://cococo.lcl/ahah/advanced_popup/myphp');
	});
	
});

})(jQuery);