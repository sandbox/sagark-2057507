<?php

/* ---- Content generators ---- */

function _advanced_popup_text($attributes, $return = FALSE){

  $title = $attributes['title'] ? $attributes['title'] : 'Text';
  if ($return == 'title'){
    return $title;
  }

  $body = $attributes['text'];
  if ($return == 'body'){
    return $body;
  }

  if(is_array($body)){
  	$body = drupal_render($body);
  }
  echo  $body;
}

function _advanced_popup_node($attributes, $return = FALSE){

  $node = node_load($attributes['node']);

  if ($node) {

	  $title = isset($attributes['title']) && $attributes['title'] ? $attributes['title'] : $node->title;
	  if ($return == 'title'){
	    return $title;
	  }

		$mode = isset($attributes['teaser']) && $attributes['teaser'] ? 'teaser' : 'full';

    $body = drupal_render(
      node_view(
        $node,
        $mode
      )
    );

	  if ($return == 'body'){
	    return $body;
	  }

	  if(is_array($body)){
	  	$body = drupal_render($body);
	  }
	  echo  $body;
	}
}


function _advanced_popup_myphp($attributes, $return = FALSE){
	
  global $user;
  $body = '$body';
  $url = $_REQUEST['url'];
  $url = str_replace_once('/', '', $url);
  $url = str_replace('http://cococo.lcl/', '', $url);

  $url = drupal_get_normal_path($url);
  //Found below method in: user_page()
  menu_set_active_item($url);
  $body = menu_execute_active_handler(NULL, FALSE);
  
  if(is_array($body)){
  	$body = drupal_render($body);
  }
  echo  $body;
}


function _advanced_popup_user($attributes, $return = FALSE){
	
  //$node = node_load($attributes['node']);
  $node = user_page(); 

  if ($node) {

	  $title = isset($attributes['title']) && $attributes['title'] ? $attributes['title'] : $node->title;
	  if ($return == 'title'){
	    return $title;
	  }

		$mode = isset($attributes['teaser']) && $attributes['teaser'] ? 'teaser' : 'full';

    $body = drupal_render(
      node_view(
        $node,
        $mode
      )
    );

	  if ($return == 'body'){
	    return $body;
	  }

	  if(is_array($body)){
	  	$body = drupal_render($body);
	  }
	  echo  $body;
	}
}



function _advanced_popup_block($attributes, $return = FALSE){

  $module = isset($attributes['module']) ? $attributes['module'] : 'block';
  $delta = isset($attributes['delta']) ? $attributes['delta'] : $attributes['block'];

  $block = module_invoke(
    $module,
    'block_view',
    $delta
  );

  $title = $attributes['title'] ?  $attributes['title'] : $block['subject'];
  $body =
    '<div class="block">' .
      (is_array($block['content']) ? drupal_render($block['content']) : $block['content']) .
    '</div>';

  if ($return == 'title'){
    return $title;
  }

  if ($return == 'body'){
    return $body;
  }

  if(is_array($body)){
  	$body = drupal_render($body);
  }
  
  echo  $body;  
}




function _advanced_popup_form($attributes, $return = FALSE){

  $title = $attributes['title'] ?  $attributes['title'] : 'form';
  if ($return == 'title'){
    return $title;
  }

  $body = drupal_render(drupal_get_form($attributes['form']));

  if ($attributes['ajax']){
    $body = '<div class="ajax-form-wrapper">' . $body . '</div>';
  }

  if ($return == 'body'){
    return $body;
  }

  if(is_array($body)){
  	$body = drupal_render($body);
  }
  echo  $body;
}




function _advanced_popup_view($attributes, $return = FALSE){

  $title = FALSE;
  $body = FALSE;

  if (module_exists('views')){

    $view = views_get_view($attributes['view']);

    $display = $attributes['display'] ? $attributes['display'] : 'default';
    $args = isset($attributes['args'])
      ? explode(',', $attributes['args'])
      : array();

    if ($view){
      $view->set_arguments($args);
      $view->set_display($display);
      $body = $view->preview();

      $computed_title = $view->get_title();
      $title = $attributes['title']
        ? $attributes['title']
        : ($computed_title
            ? $computed_title
            : @$view->display[$display]->display_options['title']
          );

    }

  } else {
    $body = false;
  }

  $title = $title ? $title : 'View';
  if ($return == 'title'){
    return $title;
  }

  if ($return == 'body'){
    return $body;
  }

  if(is_array($body)){
  	$body = drupal_render($body);
  }
  
  echo  $body;
}


function _advanced_popup_php($attributes, $return = FALSE){

  $title = $attributes['title'] ? $attributes['title'] : 'PHP';
  if ($return == 'title'){
    return $title;
  }
  $body = eval($attributes['php']);
  if ($return == 'body'){
    return $body;
  }

  if(is_array($body)){
  	$body = drupal_render($body);
  }
  
  echo  $body;
}




function _advanced_popup_menu($attributes){

  module_load_include('inc', 'advanced_popup', 'includes/advanced_popup.util');

  if (!isset($attributes['empty-body'])) {
    $attributes['empty-body'] = 'none';
  }

  $menu = menu_load($attributes['menu']);
  $children = menu_tree_all_data($attributes['menu']);
  $menu['below'] = $children ? array_filter(
      menu_tree_all_data(
        $attributes['menu']),
        '_advanced_popup_menu_visible'
      )
    : FALSE;
  $menu['link']['has_children'] = isset($children);
  $menu['link']['title'] = isset($attributes['title']) && strlen($attributes['title'])
    ? $attributes['title']
    : $menu['title'];

  return _advanced_popup_menuelement($menu, $attributes);
}


function str_replace_once($str_pattern, $str_replacement, $string)
{
	if (strpos($string, $str_pattern) !== false)
      {
		$occurrence = strpos($string, $str_pattern);
		return substr_replace($string, $str_replacement, strpos($string, $str_pattern), strlen($str_pattern));
	}
	return $string;
}