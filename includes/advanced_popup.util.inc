<?php


/* ---- AJAX callback ---- */
function advanced_popup_get_ahah($type, $attr_hash = ''){

  module_load_include('inc', 'advanced_popup', 'includes/advanced_popup.api');

  $attributes = advanced_popup_cache_attributes($attr_hash);

  $function = '_advanced_popup_' . $type;

  print $function($attributes, 'body');
  exit;
}

/* ---- AJAX caching helper ---- */

function advanced_popup_cache_attributes($cache){

  if (is_array($cache)){

    $hash = md5(serialize($cache));
    cache_set($hash, $cache, 'cache', time() + 360000);

    return $hash;

  } else {

    $cached = cache_get($cache);

    return $cached ? $cached->data : FALSE;
  }
}

